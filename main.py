from typing import Optional, List

from sympy import parse_expr, diff, Eq, solve, integrate, Expr, Symbol, Q, trigsimp, simplify, powsimp
from sympy.abc import y, u, alpha, xi

from sympy.assumptions.assume import global_assumptions

# Defining quantity of substitutions
n_substitutions: int = int(input("n_substitutions := "))

# Defining φ
inp: str = input("φ(u) := ")
phi_from_u: Expr = parse_expr(inp)

# Looking for derivative of φ
d_phi_by_du: Expr = diff(phi_from_u)
print("dφ/du = " + str(d_phi_by_du))

# Looking for inverse φ
equation: Eq = Eq(phi_from_u, y)
solutions: List[Expr] = solve(equation, u)
inverse_phi_from_u: Optional[Expr] = None
root: Expr

for root in solutions:
    if root.subs(y, phi_from_u.subs(u, 0)) == 0:
        inverse_phi_from_u = root
        break

inverse_phi_from_u: Expr = inverse_phi_from_u.subs(y, u)
print(f"φ^-1 = {str(inverse_phi_from_u)}")

# Making some assumptions
global_assumptions.add(Q.positive(alpha))
global_assumptions.add(Q.positive(xi))

# Entering f_η(u)
inp = input("f_η(u) := ")
f_eta_from_u: Expr = parse_expr(inp)

# Defining a
inp = input("a := ")
a: Expr = parse_expr(inp)

# Defining b
inp = input("b := ")
b: Expr = parse_expr(inp)

# Defining c
c: Expr = phi_from_u.subs(u, a)
print(f"c = {str(c)}")

# Defining d
d: Expr = phi_from_u.subs(u, b)
print(f"d = {str(d)}")

# Checking if ∫[a, b](f_η(u) du) = 1
norm: Expr = integrate(f_eta_from_u, (u, c, d))
print(f"Norm = {str(norm)}")
assert norm == 1
φ_decreasing: bool = phi_from_u.subs(u, 0) > phi_from_u.subs(u, 1)

for i in range(n_substitutions):
    print(f"Substitution - {i}")
    # Calculating f_ξ(u)
    f_xi_from_u: Expr

    if not φ_decreasing:
        f_xi_from_u = f_eta_from_u.subs(u, phi_from_u) * d_phi_by_du
    else:
        # φ is decreasing, swapping c and d, using its absolute value in f_ξ(u), replacing α_0 with α_0'
        alpha = 1 - Symbol("alpha")
        tmp: Expr = d
        d = c
        c = tmp
        f_xi_from_u = f_eta_from_u.subs(u, phi_from_u) * abs(d_phi_by_du)

    print(f"f_ξ(u) = {f_xi_from_u}")
    equation = Eq(powsimp(trigsimp(simplify(integrate(f_xi_from_u, (u, a, xi))))), alpha)
    print(f"{equation.lhs} = {equation.rhs}")
    solutions = solve(equation, xi)
    print(f"ξ_0 = {str(solutions)}")

    # Looking for modeling formula amongst solutions
    modeling_formula: Optional[Expr] = None

    for root in solutions:
        # print(f"---\n{root.subs(α_0, 0)}\n{root.subs(α_0, 1)}\n---\n")
        if root.subs(alpha, 0) == a and root.subs(alpha, 1) == b:
            modeling_formula = root
            break

    print(f"New modeling formula is ξ_0 = {str(modeling_formula)}")
    f_eta_from_u = f_xi_from_u

    if i == n_substitutions:
        break

    # Updating a and b
    a = inverse_phi_from_u.subs(u, a)
    print(f"a = {str(c)}")
    b = inverse_phi_from_u.subs(u, b)
    print(f"d = {str(d)}")

    # Updating c and d
    c = phi_from_u.subs(u, a)
    print(f"c = {str(c)}")
    d = phi_from_u.subs(u, b)
    print(f"d = {str(d)}")
